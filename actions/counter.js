export const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
export const DECREMENT_COUNTER = 'DECREMENT_COUNTER';
export const INPUT_DIGIT = 'INPUT_DIGIT';
export const GET_RESULT = 'GET_RESULT';

export function increment() {
  return {
    type: INCREMENT_COUNTER
  };
}

export function decrement() {
  return {
    type: DECREMENT_COUNTER
  };
}

export function inputDigit(digit) {
  return {
    type: INPUT_DIGIT,
    digit: digit
  };
}

export function getResult() {
  return {
    type: GET_RESULT
  };
}