import { INCREMENT_COUNTER, DECREMENT_COUNTER, INPUT_DIGIT, GET_RESULT } from '../actions/counter';

const initialState = [{
  accumulateValue: 0,
  currentValue: 0,
  ops: null,
  resultOp: false,
  firstOp: true
}];

export default function counter(state = initialState, action) {
  switch (action.type) {
  case INCREMENT_COUNTER:
    var accumulateValue = state[0].resultOp ? state[0].accumulateValue :
                          state[0].ops === '-'  ? state[0].accumulateValue - state[0].currentValue : state[0].accumulateValue+state[0].currentValue;

    if (state[0].resultOp && !state[0].ops && state[0].accumulateValue !== state[0].currentValue) {
      accumulateValue = state[0].accumulateValue + state[0].currentValue;
    }
    return [{
      accumulateValue: accumulateValue,
      currentValue: 0,
      ops: '+',
      resultOp: false,
      firstOp: false
    }, ...state];
  case DECREMENT_COUNTER:
    var accumulateValue = state[0].resultOp ? state[0].accumulateValue :
                          state[0].ops === '+'  ? state[0].accumulateValue + state[0].currentValue : state[0].accumulateValue-state[0].currentValue ;
    if (state[0].resultOp && !state[0].ops && state[0].accumulateValue !== state[0].currentValue) {
      accumulateValue = state[0].accumulateValue - state[0].currentValue;
    }
    accumulateValue = state[0].firstOp ? state[0].currentValue : accumulateValue;

    return [{
      accumulateValue: accumulateValue,
      currentValue: 0,
      ops: "-",
      resultOp: false,
      firstOp: false,
    }, ...state];
  case INPUT_DIGIT:
    var currentValue = state[0].resultOp ? 0 : state[0].currentValue;
    return [{
      accumulateValue: state[0].accumulateValue,
      currentValue: currentValue*10+action.digit,
      resultOp: false,
      ops: state[0].ops,
      firstOp: state[0].firstOp
    }, ...state];
  case GET_RESULT:
    if (state[0].resultOp)
      return state;
    var accumulate = state[0].accumulateValue;
    if (state[0].ops) {
      accumulate = (state[0].ops === '+') ? state[0].accumulateValue + state[0].currentValue :
                                        state[0].accumulateValue - state[0].currentValue;
    }
    return [{
      accumulateValue: accumulate,
      currentValue: accumulate,
      ops: null,
      resultOp: true,
      firstOp: state[0].firstOp
    }, ...state]
  default:
    return state;
  }
}
