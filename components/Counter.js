import React, { Component, PropTypes } from 'react';

class Counter extends Component {
  render() {
    const { increment, decrement, inputDigit, getResult, counter } = this.props;
    console.log(counter);
    var logItems=counter.map(c => (
      <li>Operation: {c.ops ? c.ops : "Result or Input"} Accumulate {c.accumulateValue} Input: {c.currentValue}</li>
    ));
    return (
      <div>
        <div className="calc-wrapper">
          <input type="text" value={counter[0].currentValue} ref="Result" readonly />
            <div className="buttons">
              <div className="button" onClick={getResult}>=</div>
              <div className="clear"></div>
              <div className="button" onClick={inputDigit.bind(this, 1)}>1</div>
              <div className="button" onClick={inputDigit.bind(this, 2)}>2</div>
              <div className="button" onClick={inputDigit.bind(this, 3)}>3</div>
              <div className="clear"></div>
              <div className="button" onClick={inputDigit.bind(this, 4)}>4</div>
              <div className="button" onClick={inputDigit.bind(this, 5)}>5</div>
              <div className="button" onClick={inputDigit.bind(this, 6)}>6</div>
              <div className="clear"></div>
              <div className="button" onClick={inputDigit.bind(this, 7)}>7</div>
              <div className="button" onClick={inputDigit.bind(this, 8)}>8</div>
              <div className="button" onClick={inputDigit.bind(this, 9)}>9</div>
              <div className="clear"></div>
              <div className="button" onClick={increment}>+</div>
              <div className="button" onClick={inputDigit.bind(this, 0)}>0</div>
              <div className="button" onClick={decrement}>-</div>
              <div className="clear"></div>
            </div>
          </div>
          <ul>
            Log operation:
            {logItems}
          </ul>
        </div>
    );
  }
}

Counter.propTypes = {
  increment: PropTypes.func.isRequired,
  decrement: PropTypes.func.isRequired,
  inputDigit: PropTypes.func.isRequired,
  getResult: PropTypes.func.isRequired,
  counter: PropTypes.number.isRequired
};

export default Counter;
